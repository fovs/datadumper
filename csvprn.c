/*  csvprn.c - Printer for the CSV output format
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "csvprn.h"
#include "converter.h"

struct output_printer csvprn_get(void)
{
	struct output_printer printer = {
		header : csvprn_header,
		footer : csvprn_footer,
		fiberaccel : csvprn_fiberaccel,
		adxlaccel : csvprn_adxlaccel,
		hirestemp : csvprn_hirestemp,
		lorestemp : csvprn_lorestemp,
		padding : csvprn_padding
	};
	return printer;
}

void csvprn_header(FILE *output)
{
	fprintf(output, "%s,%s," \
		"%s,%s,%s," \
		"%s,%s,%s," \
		"%s,%s,%s," \
		"%s,%s,%s," \
		"%s,%s,%s," \
		"%s,%s," \
		"%s,%s,%s,%s,%s",
		"Time_Counter", "Lift_Off",
		"Fiber_A_pri", "Fiber_A_sec",
		"Fiber_B_pri", "Fiber_B_sec",
		"Fiber_C_pri", "Fiber_C_sec",
		"Fiber_D_pri", "Fiber_D_sec",
		"ADXL_X", "ADXL_Y", "ADXL_Z",
		"Fiber_A_temp", "Fiber_B_temp", "Fiber_C_temp", "Fiber_D_temp",
		"LoO", "PTA",
		"LM74local", "LM74adxl", "uCtemp", "ADC25ref", "ADC25pwr");
}

void csvprn_footer(FILE *output)
{
	fprintf(output, "\n");
}

void csvprn_fiberaccel(FILE *output, void *record)
{
	uint8_t lift_off;
	uint32_t counter;
	uint16_t fiberApri, fiberAsec, fiberBpri, fiberBsec,
	    fiberCpri, fiberCsec, fiberDpri, fiberDsec;
	convert_fiberaccel(record, &lift_off, &counter,
	    &fiberApri, &fiberAsec,
	    &fiberBpri, &fiberBsec,
	    &fiberCpri, &fiberCsec,
	    &fiberDpri, &fiberDsec);

	fprintf(output, "\n%lu,%u," \
		"%hu,%hu," \
		"%hu,%hu," \
		"%hu,%hu," \
		"%hu,%hu",
		counter, lift_off ? 1 : 0,
		fiberApri, fiberAsec,
		fiberBpri, fiberBsec,
		fiberCpri, fiberCsec,
		fiberDpri, fiberDsec);
}

void csvprn_adxlaccel(FILE *output, void *record)
{
	int16_t x, y, z;
	convert_adxlaccel(record, &x, &y, &z);
	fprintf(output, ",%hd,%hd,%hd", x, y, z);
}

void csvprn_hirestemp(FILE *output, void *record)
{
	uint16_t fiberAtemp, fiberBtemp, fiberCtemp, fiberDtemp;
	uint16_t loss_of_output, pump_temperature_alarm;
	convert_hirestemp(record, &fiberAtemp, &fiberBtemp, &fiberCtemp,
	    &fiberDtemp, &loss_of_output, &pump_temperature_alarm);

	fprintf(output, ",,,,%hu,%hu,%hu,%hu,%hu,%hu",
		fiberAtemp, fiberBtemp, fiberCtemp, fiberDtemp,
		loss_of_output, pump_temperature_alarm);
}

void csvprn_lorestemp(FILE *output, void *record)
{
	int16_t lm74local, lm74adxl;
	uint16_t uCtemp;
	int16_t adc25ref, adc25pwr;
	convert_lorestemp(record, &lm74local, &lm74adxl, &uCtemp,
	    &adc25ref, &adc25pwr);
	fprintf(output, ",,,,,,,,,,%hd,%hd,%hu,%hd,%hd",
		lm74local, lm74adxl, uCtemp, adc25ref, adc25ref);
}

void csvprn_padding(FILE *output, void *record, const uint8_t to_skip)
{
	/* empty */
}
