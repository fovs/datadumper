/*  converter.h
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONVERTER_H
#define CONVERTER_H

#include <inttypes.h>

void convert_fiberaccel(void *record, uint8_t *lift_off, uint32_t *counter,
    uint16_t *fiberApri, uint16_t *fiberAsec,
    uint16_t *fiberBpri, uint16_t *fiberBsec,
    uint16_t *fiberCpri, uint16_t *fiberCsec,
    uint16_t *fiberDpri, uint16_t *fiberDsec);

void convert_hirestemp(void *record, uint16_t *fiberAtemp, uint16_t *fiberBtemp,
    uint16_t *fiberCtemp, uint16_t *fiberDtemp, uint16_t *loss_of_output,
    uint16_t *pump_temperature_alarm);

void convert_adxlaccel(void *record, int16_t *x, int16_t *y, int16_t *z);

void convert_lorestemp(void *record, int16_t *lm74local, int16_t *lm74adxl,
    uint16_t *uCtemp, int16_t *adc25ref, int16_t *adc25pwr);


#endif /* CONVERTER_H */
