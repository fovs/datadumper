/*  abstract_printer.h - The generic interface for a data record printer
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACT_PRINTER_H
#define ABSTRACT_PRINTER_H

struct output_printer
{
	void (*header)(FILE *output);
	void (*footer)(FILE *output);
	void (*fiberaccel)(FILE *output, void *record);
	void (*adxlaccel)(FILE *output, void *record);
	void (*hirestemp)(FILE *output, void *record);
	void (*lorestemp)(FILE *output, void *record);
	void (*padding)(FILE *output, void *record, const uint8_t to_skip);
};

#endif
