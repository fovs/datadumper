/*  datadumper.c - A program to convert dense/packed files from experiment
 *                 FOVS on REXUS 15 to human readable or simpler machine
 *                 readable data formats
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <boost/crc.hpp>      // for boost::crc_basic, boost::crc_optimal
#include <boost/cstdint.hpp>  // for boost::uint16_t
#include "sdcard_record_types.h"
#include "abtract_printer.h"
#include "tableprn.h"
#include "csvprn.h"
#include "hexprn.h"
#include "converter.h"

#define BIGBUFFER_SIZE 8192
#define FIBACC_SIZE (sizeof(union fiber_acceleration))
#define REFACC_SIZE (sizeof(union reference_acceleration))
#define HITEMP_SIZE (sizeof(union hires_temperature))
#define LOTEMP_SIZE (sizeof(union lores_temperature))

enum output_printer_format {
	RawHex, CSV, RealUnit
};


void parse_args(const int argc, char **const argv,
		const char **const error_msg,
		bool *const is_help, const char **const input_file,
		const char **const output_file,
		enum output_printer_format *const format);
void enqueue_byte(FILE *output, struct output_printer *printer, uint8_t c);
uint16_t calc_crc(const uint8_t* buf, uint8_t len);
void print_crc_check_failed_error(const char *const record_type,
				  const uint16_t is_crc,
				  const uint16_t shouldbe_crc,
				  const uint32_t current_byte_idx);

int main(int argc, char **argv)
{
	bool is_help;
	const char *error_msg;
	const char *input_filename;
	const char *output_filename;
	enum output_printer_format selected_printer;
	parse_args(argc, argv, &error_msg, &is_help, &input_filename,
		   &output_filename, &selected_printer);

	if (is_help) {
		fprintf(stderr,
		    "Usage:\n" \
		    "  $ datadumper " \
		    "([-h] | [-i input_file] [-o output_file]" \
		    " [-f printer_format])\n" \
		    "Options:\n" \
		    "%-16s%s\n" \
		    "%-16s%s\n" \
		    "%-16s%s\n" \
		    "%-16s%s\n" \
		    "\n",
		    "-h", "Print this help.",
		    "-i input_file",
		    "Read from input_file, or stdin if omitted.",
		    "-o output_file",
		    "Dump data to output_file, or stdout if omitted.",
		    "-f printer_format",
		    "A selector of RawHex, CSV, RealUnit to specify the" \
		    " layout of the output.");
		return 0;
	} else if (error_msg != 0) {
		fprintf(stderr, "Error: \'%s\'. Exiting.\n", error_msg);
		return -1;
	}

	struct output_printer printer;
	switch (selected_printer) {
	case RawHex:
		printer = hexprn_get();
		break;
	case CSV:
		printer = csvprn_get();
		break;
	case RealUnit:
		printer = tableprn_get();
		break;
	}

	FILE* input;
	FILE* output;
	if (strncmp(input_filename, "-", 2) == 0) {
		input = stdin;
	} else {
		input = fopen(input_filename, "r");
		if (input == NULL) {
			perror("Could not open input file for reading");
			return -1;
		}
	}

	if ((strncmp(output_filename, "-", 2) == 0) ||
	    (*output_filename == 0 && input == stdin)) {
		output = stdout;
	} else {
		if (*output_filename == 0) {
			char *filename =
			    (char*) malloc(strlen(input_filename) + 5);
			if (filename == 0) {
				perror("Could not copy the filename");
				return -1;
			}

			strcpy(filename, input_filename);
			switch(selected_printer) {
			case RawHex:
				strcat(filename, ".hex");
				break;
			case CSV:
				strcat(filename, ".csv");
				break;
			case RealUnit:
				strcat(filename, ".unt");
				break;
			}

			output = fopen(filename, "w");
			free(filename);
		} else {
			output = fopen(output_filename, "w");
		}
		if (output == NULL) {
			perror("Could not open output file for writing");
			if (input != stdin) {
				if (fclose(input) != 0) {
					perror("Also failed to close " \
					       "the input file");
				}
			}
			return -1;
		}
	}

	// needed to detect and skip empty buffers
	uint32_t current_byte = -1;
	uint16_t skip_bytes = 0;

	uint8_t bigbuffer[BIGBUFFER_SIZE];
	printer.header(output);
	while(1) {
		const int num_read = fread(bigbuffer, sizeof(bigbuffer[0]),
					   BIGBUFFER_SIZE, input);
		for (int i = 0; i < num_read; ++i) {
			++current_byte;
			if (skip_bytes > 0) {
				--skip_bytes;
				continue;
			}
			if (current_byte % 0x2000 == 0 && bigbuffer[i] == 0) {
				skip_bytes = 0x2000 - 1;
				continue;
			}
			enqueue_byte(output, &printer, bigbuffer[i]);
		}
		if (num_read != BIGBUFFER_SIZE) {
			if (feof(input))
				break;
			perror("Failed reading the stream");
			return -1;
		}
	};
	printer.footer(output);

	bool close_failed = false;
	if (strncmp(input_filename, "-", 2)) {
		if (fclose(input)) {
			perror("Could not close the input file");
			close_failed = true;
		}
	}
	if (strncmp(output_filename, "-", 2)) {
		if (fclose(output)) {
			perror("Could not close the outpur file");
			close_failed = true;
		}
	}

	if (close_failed)
		return -1;
	return 0;
}


void parse_args(const int argc, char **const argv, const char **const error_msg,
		bool *const is_help, const char **const input_file,
		const char **const output_file,
		enum output_printer_format *const format)
{
	// load defaults
	*is_help = false;
	*input_file = "-";
	*output_file = ""; // placeholder, signals use of inputname.formatname
	*format = RawHex;
	*error_msg = 0;

	for (int i = 1; i < argc; ++i) {
		char * s = argv[i];
		if (strncmp(s, "-h", 3) == 0) {
			*is_help = true;
		} else if (strncmp(s, "-o", 3) == 0) {
			if (i + 1 < argc) {
				*output_file = argv[i + 1];
			} else {
				*error_msg = "Parameter output_file missing.";
				return;
			}
		} else if (strncmp(s, "-i", 3) == 0) {
			if (i + 1 < argc) {
				*input_file = argv[i + 1];
			} else {
				*error_msg = "Parameter input_file missing.";
				return;
			}
		} else if (strncmp(s, "-f", 3) == 0) {
			if (i + 1 < argc) {
				char *const f = argv[i + 1];
				// make all common/lower case
				for (char *p = f; *p; ++p)
					*p = tolower(*p);
				if (strncmp(f, "rawhex", 7) == 0)
					*format = RawHex;
				else if (strncmp(f, "csv", 4) == 0)
					*format = CSV;
				else if (strncmp(f, "realunit", 9) == 0)
					*format = RealUnit;
			} else {
				*error_msg = "Parameter printer_format missing.";
				return;
			}
		}
	}
}

uint16_t calc_crc(const uint8_t* buf, uint8_t len)
{
	boost::crc_optimal<16, 0x1021, 0x0000, 0, false, false>  crc_ccitt;
	crc_ccitt.process_bytes(buf, len);
	return crc_ccitt.checksum();
//	return crc_ccitt(0, buf, len);
}

void print_crc_check_failed_error(const char *const record_type,
				  const uint16_t is_crc,
				  const uint16_t shouldbe_crc,
				  const uint32_t current_pos)
{
	fprintf(stderr, "CRC verification failed for %s at 0x%08x: " \
		"is 0x%04x, should be 0x%04x.\n",
		record_type, current_pos, is_crc, shouldbe_crc);
}
void enqueue_byte(FILE *output, struct output_printer *printer, uint8_t c)
{
	static uint8_t buffer[FIBACC_SIZE];
	static uint32_t current_byte_idx = -sizeof(buffer) + 1;
	static uint8_t wait_next_bytes = sizeof(buffer) - 1;

	for (uint8_t i = 0; i < sizeof(buffer) - 1; ++i)
		buffer[i] = buffer[i+1];
	buffer[sizeof(buffer) - 1] = c;

	if (wait_next_bytes > 0) {
		++current_byte_idx;
		--wait_next_bytes;
		return;
	}

	const uint8_t id = (buffer[0] >> 5) & 0x07;
	if (id == FIBER_ACCELERATION_RECORD_ID) {
		const uint16_t shouldbe_crc = calc_crc(buffer, FIBACC_SIZE - 2);
		const uint16_t is_crc = (buffer[FIBACC_SIZE-2] << 8)
				      | buffer[FIBACC_SIZE-1];
		if (shouldbe_crc == is_crc) {
			// to check & report "strange" counters
			// (e.g. current is less than prev)
			static uint32_t previous_counter = 0;
			uint32_t now_counter;
			uint16_t do_not_use16;
			uint8_t do_not_use8;
			convert_fiberaccel(buffer, &do_not_use8, &now_counter,
					   &do_not_use16, &do_not_use16,
					   &do_not_use16, &do_not_use16,
					   &do_not_use16, &do_not_use16,
					   &do_not_use16, &do_not_use16);
			if (previous_counter >= now_counter) {
				fprintf(stderr,
					"Strange Counter detected at 0x%08x: " \
					"prev was 0x%08x, now is 0x%08x\n",
					current_byte_idx, previous_counter,
					now_counter);
			}
			previous_counter = now_counter;

			printer->fiberaccel(output, buffer);
			wait_next_bytes = FIBACC_SIZE - 1;
		} else {
			print_crc_check_failed_error("union fiber_acceleration",
						     is_crc, shouldbe_crc,
						     current_byte_idx);
		}
	} else if (id == REFERENCE_ACCELERATION_RECORD_ID) {
		const uint16_t shouldbe_crc = calc_crc(buffer, REFACC_SIZE - 2);
		const uint16_t is_crc = (buffer[REFACC_SIZE-2] << 8)
				      | buffer[REFACC_SIZE-1];
		if (shouldbe_crc == is_crc) {
			printer->adxlaccel(output, buffer);
			wait_next_bytes = REFACC_SIZE - 1;
		} else {
			print_crc_check_failed_error("union reference_acceleration",
						     is_crc, shouldbe_crc,
						     current_byte_idx);
		}
	} else if (id == HIRES_TEMPERATURE_RECORD_ID) {
		const uint16_t shouldbe_crc = calc_crc(buffer, HITEMP_SIZE - 2);
		const uint16_t is_crc = (buffer[HITEMP_SIZE-2] << 8)
				      | buffer[HITEMP_SIZE-1];
		if (shouldbe_crc == is_crc) {
			printer->hirestemp(output, buffer);
			wait_next_bytes = HITEMP_SIZE - 1;
		} else {
			print_crc_check_failed_error("union hires_temperature",
						     is_crc, shouldbe_crc,
						     current_byte_idx);
		}
	} else if (id == LORES_TEMPERATURE_RECORD_ID) {
		const uint16_t shouldbe_crc = calc_crc(buffer, LOTEMP_SIZE - 2);
		const uint16_t is_crc = (buffer[LOTEMP_SIZE-2] << 8)
				      | buffer[LOTEMP_SIZE-1];
		if (shouldbe_crc == is_crc) {
			printer->lorestemp(output, buffer);
			wait_next_bytes = LOTEMP_SIZE - 1;
		} else {
			print_crc_check_failed_error("union lores_temperature",
						     is_crc, shouldbe_crc,
						     current_byte_idx);
		}
	} else {
		const uint32_t chunk_free_bytes =
		    0x2000 - (current_byte_idx % 0x2000);
		const uint8_t max_pad = FIBACC_SIZE + HITEMP_SIZE;
		if (id == EMPTY_RECORD_ID && chunk_free_bytes <= max_pad) {
			wait_next_bytes = chunk_free_bytes;
//			--wait_next_bytes; // -1 because of "one off"
			printer->padding(output, buffer, wait_next_bytes);
			// -1 because that's the current byte
			--wait_next_bytes;
#define DEBUG
		} else {
#ifdef DEBUG
			fprintf(stderr, "Current buffer: 0x");
			for (uint8_t i = 0; i < sizeof(buffer); ++i)
				fprintf(stderr, "%02x", buffer[i]);
			fprintf(stderr, "\n");
#endif
			if (id == EMPTY_RECORD_ID && chunk_free_bytes >= max_pad)
				fprintf(stderr, "Is Avail: %d (0x%x)\n", chunk_free_bytes, chunk_free_bytes);
			fprintf(stderr, "Reject orphaned byte \'0x%02x\' at " \
				"position %d (0x%08x).\n",
				buffer[0], current_byte_idx, current_byte_idx);
		}
	}
	++current_byte_idx;
}

