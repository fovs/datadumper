/*  csvprn.h
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CSVPRN_H
#define CSVPRN_H

#include <stdio.h>
#include <inttypes.h>
#include "abtract_printer.h"

struct output_printer csvprn_get(void);
void csvprn_header(FILE *output);
void csvprn_footer(FILE *output);
void csvprn_fiberaccel(FILE *output, void *record);
void csvprn_adxlaccel(FILE *output, void *record);
void csvprn_hirestemp(FILE *output, void *record);
void csvprn_lorestemp(FILE *output, void *record);
void csvprn_padding(FILE *output, void *record, const uint8_t to_skip);

#endif
