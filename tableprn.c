/*  tableprn.c - Print partially converted field values in human readable table
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tableprn.h"
#include "sdcard_record_types.h"
#include "converter.h"
#include <math.h>

struct output_printer tableprn_get(void)
{
	struct output_printer printer = {
		header : tableprn_header,
		footer : tableprn_footer,
		fiberaccel : tableprn_fiberaccel,
		adxlaccel : tableprn_adxlaccel,
		hirestemp : tableprn_hirestemp,
		lorestemp : tableprn_lorestemp,
		padding : tableprn_padding
	};
	return printer;
}

void tableprn_header(FILE *output)
{
}

void tableprn_footer(FILE *output)
{
	fprintf(output, "\n");
}

void tableprn_fiberaccel(FILE *output, void *record)
{
	uint8_t lift_off;
	uint32_t counter;
	uint16_t fiberApri, fiberAsec;
	uint16_t fiberBpri, fiberBsec;
	uint16_t fiberCpri, fiberCsec;
	uint16_t fiberDpri, fiberDsec;

	convert_fiberaccel(record, &lift_off, &counter, &fiberApri, &fiberAsec,
	    &fiberBpri, &fiberBsec, &fiberCpri, &fiberCsec,
	    &fiberDpri, &fiberDsec);
	fprintf(output, "\n(A) | %11.6fs | %2s | " \
		"A: %5u %5u  B: %5u %5u  " \
		"C: %5u %5u  D: %5u %5u |",
		round(counter * 30.0 * 10000.0/(12.0 * 384.0)) / 1000000.0,
		lift_off ? "" : "LO",
		fiberApri, fiberAsec,
		fiberBpri, fiberBsec,
		fiberCpri, fiberCsec,
		fiberDpri, fiberDsec);
}

void tableprn_adxlaccel(FILE *output, void *record)
{
	int16_t x, y, z;
	convert_adxlaccel(record, &x, &y, &z);
	fprintf(output, " (B) | " \
		"X: %+2.3fg  Y: %+2.3fg  Z: %+2.3fg |",
		round(x * 2.9) / 1000.0,
		round(y * 2.9) / 1000.0,
		round(z * 2.9) / 1000.0);
}

static inline double ntc_adc_to_celsius(const uint16_t adc)
{
	const uint32_t pullup = 68000;
	const uint16_t B = 3380;
	const long double resistance = (pullup * 2.5L * adc)
					/ (3.3L * 0xffff - 2.5L * adc);
	const long double millikelvin =
	    B * 298.15 * 1000.0L / (B + logl(resistance / 10000.0) * 298.15);
	const long double millicelsius = millikelvin - 273150.0;
	const double celsius = round(millicelsius) / 1000.0;
	return celsius;
}

void tableprn_hirestemp(FILE *output, void *record)
{
	uint16_t fiberA, fiberB, fiberC, fiberD;
	uint16_t loss_of_output, pump_temperature_alarm;
	convert_hirestemp(record, &fiberA, &fiberB, &fiberC, &fiberD,
	    &loss_of_output, &pump_temperature_alarm);

	fprintf(output, " (C) | " \
		"A: %+3.3f°C  B: %+3.3f°C  C: %+3.3f°C  D: %+3.3f°C  " \
		"LoO: %2s PTA: %2s |", \
		ntc_adc_to_celsius(fiberA),
		ntc_adc_to_celsius(fiberB),
		ntc_adc_to_celsius(fiberC),
		ntc_adc_to_celsius(fiberD),
		(loss_of_output & (1<<15)) ? "ON" : "--",
		(pump_temperature_alarm & (1<<15)) ? "ON" : "--");
}

void tableprn_lorestemp(FILE *output, void *record)
{
	int16_t lm74local, lm74adxl;
	uint16_t uCtemp;
	int16_t adc25ref, adc25pwr;
	convert_lorestemp(record, &lm74local, &lm74adxl, &uCtemp,
	    &adc25ref, &adc25pwr);
	fprintf(output, " (D) | " \
		"LM74 local: %+3.3f°C adxl: %+3.3f°C  " \
		"uCtemp: %4u  ADCref: %+1.3fV pwr: %+1.3fV |",
		round(lm74local * 62.5) / 1000.0,
		round(lm74adxl * 62.5) / 1000.0,
		(uint16_t) uCtemp,
		round((adc25ref * 2 * 3.3) / ((2047+1) * 1.6) * 1000.0) / 1000.0,
		round((adc25pwr * 2 * 3.3) / ((2047+1) * 1.6) * 1000.0) / 1000.0);
}

void tableprn_padding(FILE *output, void *record, const uint8_t to_skip)
{
	const uint8_t byte = *(uint8_t*)record;

	if ((to_skip & 0x1f) == (byte & 0x1f)) {
		fprintf(output, " (E) OK  pad %3d |", to_skip);
	} else {
		fprintf(output,
			" (E) ERR expect: 0x%02x (=0x%02x & 0x1f)  " \
			"is: 0x%02x (=0x%02x & 0x1f) |",
			to_skip & 0x1f, to_skip, byte & 0x1f, byte);
	}
}
