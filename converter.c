/*  converter.c - Functions to convert the packed fields in the data records
 *                to normal C data types
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "converter.h"
#include "sdcard_record_types.h"

void convert_fiberaccel(void *record, uint8_t *lift_off, uint32_t *counter,
    uint16_t *fiberApri, uint16_t *fiberAsec,
    uint16_t *fiberBpri, uint16_t *fiberBsec,
    uint16_t *fiberCpri, uint16_t *fiberCsec,
    uint16_t *fiberDpri, uint16_t *fiberDsec)
{
	const union fiber_acceleration *const r =
	    (union fiber_acceleration*) record;
	*lift_off = (r->e.id__reserved__lift_off__counter_26_24 >> 3) & 1;
	uint32_t c;
	c= r->e.id__reserved__lift_off__counter_26_24 & 0x7;
	c= (c<< 8) | r->e.counter_23_16;
	c= (c<< 8) | r->e.counter_15_8;
	c= (c<< 8) | r->e.counter_7_0;
	*counter = c;

	uint16_t Apri = r->e.fiberApri_15_8;
	Apri = (Apri << 8) | r->e.fiberApri_7_0;
	uint16_t Asec = r->e.fiberAsec_15_8;
	Asec = (Asec << 8) | r->e.fiberAsec_7_0;
	*fiberApri = Apri;
	*fiberAsec = Asec;

	uint16_t Bpri = r->e.fiberBpri_15_8;
	Bpri = (Bpri << 8) | r->e.fiberBpri_7_0;
	uint16_t Bsec = r->e.fiberBsec_15_8;
	Bsec = (Bsec << 8) | r->e.fiberBsec_7_0;
	*fiberBpri = Bpri;
	*fiberBsec = Bsec;

	uint16_t Cpri = r->e.fiberCpri_15_8;
	Cpri = (Cpri << 8) | r->e.fiberCpri_7_0;
	uint16_t Csec = r->e.fiberCsec_15_8;
	Csec = (Csec << 8) | r->e.fiberCsec_7_0;
	*fiberCpri = Cpri;
	*fiberCsec = Csec;

	uint16_t Dpri = r->e.fiberDpri_15_8;
	Dpri = (Dpri << 8) | r->e.fiberDpri_7_0;
	uint16_t Dsec = r->e.fiberDsec_15_8;
	Dsec = (Dsec << 8) | r->e.fiberDsec_7_0;
	*fiberDpri = Dpri;
	*fiberDsec = Dsec;
}

void convert_adxlaccel(void *record, int16_t *x, int16_t *y, int16_t *z)
{
	const union reference_acceleration *const r =
	    (union reference_acceleration*) record;
	uint16_t _x, _y, _z;
	_x = r->e.dimx_7_0;
	_x |= r->e.dimx_15_8 << 8;
	*x = _x;

	_y = r->e.dimy_7_0 ;
	_y |= r->e.dimy_15_8 << 8;
	*y = _y;

	_z = r->e.dimz_7_0;
	_z |= r->e.dimz_15_8 << 8;
	*z = _z;
}

void convert_hirestemp(void *record, uint16_t *fiberAtemp, uint16_t *fiberBtemp,
    uint16_t *fiberCtemp, uint16_t *fiberDtemp, uint16_t *loss_of_output,
    uint16_t *pump_temperature_alarm)
{
	const union hires_temperature *const r =
	    (union hires_temperature*) record;
	*fiberAtemp = (r->e.fiberAtemp_15_8 << 8) | r->e.fiberAtemp_7_0;
	*fiberBtemp = (r->e.fiberBtemp_15_8 << 8) | r->e.fiberBtemp_7_0;
	*fiberCtemp = (r->e.fiberCtemp_15_8 << 8) | r->e.fiberCtemp_7_0;
	*fiberDtemp = (r->e.fiberDtemp_15_8 << 8) | r->e.fiberDtemp_7_0;
	*loss_of_output = (r->e.loss_of_output_15_8 << 8)
			| r->e.loss_of_output_7_0;
	*pump_temperature_alarm = (r->e.pump_temperature_alarm_15_8 << 8)
				| r->e.pump_temperature_alarm_7_0;
}

void convert_lorestemp(void *record, int16_t *lm74local, int16_t *lm74adxl,
    uint16_t *uCtemp, int16_t *adc25ref, int16_t *adc25pwr)
{
	const union lores_temperature *const r =
	    (union lores_temperature*) record;
	uint16_t lm74l, lm74a;
	lm74l = ((r->e.res_2_0__lm74local_12_8 & 0x1f) << 8)
	      | r->e.lm74local_7_0;
	if (lm74l & (1<<12))
		lm74l |= 0xe000;
	*lm74local = lm74l;

	lm74a = ((r->e.res_2_0__lm74adxl_12_8 & 0x1f) << 8)
	      | r->e.lm74adxl_7_0;
	if (lm74a & (1<<12))
		lm74a |= 0xe000;
	*lm74adxl = lm74a;

	*uCtemp = ((r->e.res_3_0__uCtemp_11_8 & 0x0f) << 8)
		| r->e.uCtemp_7_0;
	// do not check+add sign for uCtemp because its unsigned

	*adc25ref = ((r->e.res_3_0__adc25ref_11_8 & 0x0f) << 8)
		  | r->e.adc25ref_7_0;
	if (*adc25ref & (1<<11))
		*adc25ref |= 0xe000;

	*adc25pwr = ((r->e.res_3_0__adc25pwr_11_8 & 0x0f) << 8)
		  | r->e.adc25pwr_7_0;
	if (*adc25pwr & (1<<11))
		*adc25pwr |= 0xe000;
}
