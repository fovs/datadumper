/*  hexprn.c - Printer of raw packets as hexadecimal numbers
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hexprn.h"
#include "sdcard_record_types.h"
#include "converter.h"

struct output_printer hexprn_get(void)
{
	struct output_printer printer = {
		header : hexprn_header,
		footer : hexprn_footer,
		fiberaccel : hexprn_fiberaccel,
		adxlaccel : hexprn_adxlaccel,
		hirestemp : hexprn_hirestemp,
		lorestemp : hexprn_lorestemp,
		padding : hexprn_padding
	};
	return printer;
}

void hexprn_header(FILE *output)
{
}

void hexprn_footer(FILE *output)
{
	fprintf(output, "\n");
}

void hexprn_fiberaccel(FILE *output, void *record)
{
	uint8_t lift_off;
	uint32_t counter;
	uint16_t fiberApri, fiberAsec;
	uint16_t fiberBpri, fiberBsec;
	uint16_t fiberCpri, fiberCsec;
	uint16_t fiberDpri, fiberDsec;

	convert_fiberaccel(record, &lift_off, &counter, &fiberApri, &fiberAsec,
	    &fiberBpri, &fiberBsec, &fiberCpri, &fiberCsec,
	    &fiberDpri, &fiberDsec);
	fprintf(output, "\n(A) | %8d (0x%07x) | %2s | " \
		"A: 0x%04x 0x%04x  B: 0x%04x 0x%04x  " \
		"C: 0x%04x 0x%04x  D: 0x%04x 0x%04x |",
		counter, counter,
		lift_off ? "" : "LO",
		fiberApri, fiberAsec,
		fiberBpri, fiberBsec,
		fiberCpri, fiberCsec,
		fiberDpri, fiberDsec);
}

void hexprn_adxlaccel(FILE *output, void *record)
{
	int16_t x, y, z;
	convert_adxlaccel(record, &x, &y, &z);
	fprintf(output, " (B) | " \
		"X: 0x%04x  Y: 0x%04x  Z: 0x%04x |",
		(uint16_t) x, (uint16_t) y, (uint16_t) z);
}

void hexprn_hirestemp(FILE *output, void *record)
{
	uint16_t fiberA, fiberB, fiberC, fiberD;
	uint16_t loss_of_output, pump_temperature_alarm;
	convert_hirestemp(record, &fiberA, &fiberB, &fiberC, &fiberD,
	    &loss_of_output, &pump_temperature_alarm);
	fprintf(output, " (C) | " \
		"A: 0x%04x  B: 0x%04x  C: 0x%04x  D: 0x%04x " \
		"LoO: 0x%04x  PTA: 0x%04x |", \
		fiberA, fiberB, fiberC, fiberD,
		loss_of_output, pump_temperature_alarm);
}

void hexprn_lorestemp(FILE *output, void *record)
{
	int16_t lm74local, lm74adxl;
	uint16_t uCtemp;
	int16_t adc25ref, adc25pwr;
	convert_lorestemp(record, &lm74local, &lm74adxl, &uCtemp,
	    &adc25ref, &adc25pwr);
	fprintf(output, " (D) | " \
		"LM74 local: 0x%04x adxl: 0x%04x  " \
		"uC temp: 0x%04x  ADCref: 0x%04x pwr: 0x%04x |",
		(uint16_t) lm74local, (uint16_t) lm74adxl,
		uCtemp, (uint16_t) adc25ref, (uint16_t) adc25pwr);
}

void hexprn_padding(FILE *output, void *record, const uint8_t to_skip)
{
	const uint8_t byte = *(uint8_t*)record;

	if ((to_skip & 0x1f) == (byte & 0x1f)) {
		fprintf(output, " (E) OK  pad %3d |", to_skip);
	} else {
		fprintf(output,
			" (E) ERR expect: 0x%02x (=0x%02x & 0x1f)  " \
			"is: 0x%02x (=0x%02x & 0x1f) |",
			to_skip & 0x1f, to_skip, byte & 0x1f, byte);
	}
}
